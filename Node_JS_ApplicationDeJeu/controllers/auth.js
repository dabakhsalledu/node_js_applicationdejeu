// external import
const nodemailer = require('nodemailer');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');//import de bcryptjs
//const { sendMail } = require('../utils/sender')
//J'ai voulu faire le fichier sender dans le dossier utils mais ca ma posed probleme avec la recuperation du code...
//J'ai essaye avec req mais ca a fait saute mon code
//Mes fonctions de validation
//boolean a retourner si le email n'est pas valide 
const Isemailnotvalid = (email) => {
    if (email == "") return true
    if (email === undefined) return true
    return false
}
//boolean a retourner si le password n'est pas valide 
const Ispasswordnotvalid = (psw) => {
    if (psw == "") return true
    if (psw === undefined) return true
    return false
}
let code = 0;
// @ROUTE POST /auth/Register
//@DESC Register a users
//@ACCESS Public
const my_users = [];
exports.register = async (req, res) => {
    const { email, password, lastname, firstname } = req.body;
    const transporter = nodemailer.createTransport({
        host: "smtp-mail.outlook.com",
        port: 587,
        auth: {
            user: 'dabakhtest@hotmail.com',
            pass: 'Shooter1999@'
        },
    })
    // Générer un code de confirmation à 6 chiffres
    let confirmationCode = Math.floor(100000 + Math.random() * 900000);
    code = confirmationCode;
    const info = await transporter.sendMail({
        from: 'Univers Game <dabakhtest@hotmail.com>',
        to: `${email}`,
        subject: "Identification a deux facteurs", // Subject line
        text: `Le code de confirmation est ${confirmationCode}`, // plain text body
    })
    console.log(`Message send : ${info.messageId}`);
    //const { code } = req
    const data = {
        'email': email,
        'password': password,
        'lastname': lastname,
        'firstname': firstname,
        'code': code,
    }
    //retourne un message d'erreur si l'utilisateur existe dans notre tableau
    for (let i = 0; i < my_users.length; i++) {
        if (my_users[i].email === email) {
            return res.send({ 'success': false, 'msg': 'Vous vous etes deja inregistres. Contacter le support technique' })
        }
    }
    //Variable stockant les infos
    // console.log(my_users)
    my_users.push(data);
    console.log(my_users)
    res.send({ 'succes': data })
}
//my_users.push(data);//ajoute l'utilisateur a notre variable qui est un tableau
exports.validateEmail = async (req, res) => {
    //let utilisateur = "";
    const { email, code } = req.body;
    for (let i = 0; i < my_users.length; i++) {
        if (my_users[i].code !== code || my_users[i].email !== email) {
            return res.send({ 'success': false, 'msg': 'veuillez entrer les bonnes informations(noms ou/et courriels' })
        }
    }
    const filters = { 'email': email };
    const chercheur = await User.findOne(filters);
    //On verifie si l'utilisateur n'existe pas dans la base de donnees par son email(Primary key)
    if (chercheur === true) {
        return res.send({ 'success': false, 'message': 'Vous avez deja un compte, veuillez contacter un administrateur' })
    }
    //Permet d'encrypter le mot de passe par hash 10 = nbre de fois, je lai remis dans le model
    // const salt = await bcrypt.genSalt(10);
    // const bcryptPassword = await bcrypt.hash(utilisateur.password, salt);
    //Renvoi a l'utilisateur un token apres connexion
    const token = jwt.sign(
        { id: User._id },
        process.env.JWT_SECRET_KEY,
        { expiresIn: process.env.JWT_EXPIRE }
    );
    await User.create(my_users)
    res.send({ 'success': true, 'token': token });
    // res.send({ 'success': true, 'msg': bcryptPassword });
    //console.log(token)
}
// @ROUTE POST /auth/login
//@DESC login a user
//@ACCESS Public
exports.login = async (req, res) => {
    const { email, password } = req.body;
    if (Isemailnotvalid(email)) return res.send('Informations invalides');
    if (Ispasswordnotvalid(password)) return res.send('Informations invalides');
    const filters = { 'email': email }
    const user = await User.findOne(filters);
    if (user === null) {//ou (!user)
        return res.send({ 'success': false, 'msg': 'Informations invalides' })
    }
    //compare le mot de passe encrypte avec celui mis en parametre
    // const a = await user.isMatchPassword(password)
    // console.log(a);
    const isMatch = await bcrypt.compare(password, user.password)
    console.log(isMatch)
    // const isMatch = await user.isMatchPassword(password);
    if (!isMatch) {
        return res.send({ 'success': false, 'msg': 'Information invalide' })
    }
    //Renvoi a l'utilisateur un token apres connexion
    const token = jwt.sign(
        { id: user._id },
        process.env.JWT_SECRET_KEY,
        { expiresIn: process.env.JWT_EXPIRE }
    );
    res.send({ 'success': true, 'token': token });
}

