// // @ROUTE POST /auth/login
const users = require('../models/user.js');
//ROUTE GET/users
//DESC get all users
//ACCESS Private
exports.getusers = async (req, res) => {
    const all = await users.find();
    res.send({ 'success': true, 'data': all });
}
//@Route get /users/me
//@ Desc Return information from student with
//Access Private
exports.getMe = async (req, res) => {
    const { employeeId } = req;

    const filters = { '_id': employeeId }
    const user_updated = await users.findOne(filters);

    console.log(user_updated)
    res.send({ 'success': true, 'data': user_updated });
}
//ROUTE PUT/users/me
//DESC update one user
//ACCESS Private
exports.putme = async (req, res) => {
    const { lastname, password } = req.body;
    const { userid } = req;
    const filter = { '_id': userid }
    const userPUT = await users.updateOne(filter, req.body)
    res.send({ 'success': true })
}
//ROUTE DELETE/users/me
//DESC delete one user
//ACCESS Private
exports.deleteme = async (req, res) => {
    const { userid } = req;
    const filter = { '_id': userid }
    await users.deleteOne(filter);
    res.send({ 'success': true })
}
//ROUTE PUT/users/me/reset-me
//DESC update game one user
//ACCESS Private
exports.resetme = async (req, res) => {
    const { userid } = req;
    const filter = { '_id': userid }
    await users.updateOne(filter, { wins: 0, losts: 0 })
    res.send({ 'success': true })
}
