const User = require('../models/user');
// ROUTE
//DESC ADD WINNER
//ACCESS PRIVATE
exports.winner = async (req, res) => {
    const { userid } = req;
    const { filter } = { '_id': userid };
    const utilisateur = await User.findOne(filter);
    let victoire = utilisateur.wins + 1;
    await User.updateOne(filter, { 'wins': victoire })
    console.log(victoire)
    res.send({ 'success': true })
}
// ROUTE
//DESC ADD LOST
//ACCESS PRIVATE
exports.loser = async (req, res) => {
    const { userid } = req;
    const { filter } = { '_id': userid };
    const utilisateur = await User.findOne(filter);
    let defaite = utilisateur.losts + 1;
    await User.updateOne(filter, { 'losts': defaite })
    res.send({ 'success': true })
}