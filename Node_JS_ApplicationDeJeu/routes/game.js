//External imports
const { Router } = require('express');

//Internal Imports
const { winner, loser } = require('../controllers/game')
//Auth Protect jsonwebtoken
const { protect } = require('../middlewares/auth')
//Le protect devant notre methode permet de rendre PRIVATE notre Root...
//On a fait appel a notre middleware
const router = Router({ mergeParams: true });
router.route('/win').post(protect, winner);
router.route('/lost').post(protect, loser);
module.exports = router;