//External imports
const { Router } = require('express');
//Internal Imports
const { getusers, getMe, putme, deleteme, resetme } = require('../controllers/users')
//Auth Protect jsonwebtoken
const { protect } = require('../middlewares/auth')
const router = Router({ mergeParams: true });
//Le protect devant notre methode permet de rendre PRIVATE notre Root...
//On a fait appel a notre middleware
router.route('/').get(protect, getusers);
router.route('/me').get(protect, getMe)
router.route('/me').put(protect, putme);
router.route('/me').delete(protect, deleteme);
router.route('/reset-stats').put(protect, resetme);
module.exports = router;