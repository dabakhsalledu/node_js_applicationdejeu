//External imports
const { Router } = require('express');

//Internal Imports
const { register, login, validateEmail } = require('../controllers/auth')
//Auth Protect jsonwebtoken
const { protect } = require('../middlewares/auth')

const router = Router({ mergeParams: true });
router.route('/register').post(register);
router.route('/validate-email').post(validateEmail);
router.route('/login').post(login);
// router.route('/me').get(protect, getMe)
module.exports = router;