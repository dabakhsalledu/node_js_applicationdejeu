const jwt = require('jsonwebtoken');
exports.protect = (req, res, next) => {
    const { authorization } = req.headers;
    let token;
    if (authorization !== undefined && authorization.startsWith('Bearer')) {
        token = authorization.split(' ')[1];
    }
    //console.log(token)
    // if (token === undefined) {
    if (!token) {
        return res.send({ 'success': false, 'msg': 'Not authorized to access to this route' });
    }
    //if invalid token
    // res.send({ 'success': false, 'msg': 'Not authorized to access to this route' })

    try {//verify, decrypte le token et 
        const decodeToken = jwt.verify(token, process.env.JWT_SECRET_KEY);
        req.employeeId = decodeToken.id;
    } catch (error) {
        return res.send({
            'success': false, 'msg': "not authorise"
        })
    }

    next();
}