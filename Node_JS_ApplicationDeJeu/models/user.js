const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const userSchemas = new mongoose.Schema({
    email: {
        type: String,
        require: [true, 'Email is required'],
        trim: true
    },
    password: {
        type: String,
        require: [true, 'Password is required'],
        trim: true
    },
    lastname: {
        type: String
    },
    wins: {
        type: Number,
        default: 0
    },
    losts: {
        type: Number,
        default: 0
    }
})
userSchemas.pre('save', async function () {
    const salt = await bcrypt.genSalt(10);
    console.log(this.password)
    this.password = await bcrypt.hash(this.password, salt);
    console.log(this.password)
})

userSchemas.methods.isMatchPassword = async function (password) {
    return await bcrypt.compare(password, this.password)
}

userSchemas.methods.getJsonWebToken = function () {
    return jwt.sign(
        { id: this._id },
        process.env.JWT_SECRET_KEY,
        { expiresIn: process.env.JWT_EXPIRE }
    );
}

module.exports = mongoose.model('users', userSchemas);