//External Imports
const express = require('express');
const dotenv = require('dotenv');
const connectDb = require('./config/database');
//dotenv
dotenv.config({ path: './config.env' })
//Variable
const app = express();
connectDb();
//body parse
app.use(express.json());
//Initial 
app.get('/', (req, res) => {
    res.send('Initial point');
})
//Routes and mounts
const authRouter = require('./routes/auth');
app.use('/auth', authRouter)
const useRouter = require('./routes/users');
app.use('/users', useRouter)
const gameRouter = require('./routes/game');
app.use('/game', gameRouter)
//Listener 
app.listen(process.env.PORT, () => {
    console.log(`Server listen on port : ${process.env.PORT}`);
})